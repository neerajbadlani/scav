import os
# from string import rstrip
import re
import logging
import progressbar
from time import sleep
import ast
import argparse
import json
import pprint


#
# Initial Setup
#
STACK_DEPTH=3
CSCOPE_DB = None
function_ignore_set = set()
gnu_function_ignore_set = set()
# default name is cscope.out
def set_the_name_of_cscope_db(cscope_db="cscope.out"):
    return cscope_db ; 

def build_cscope_db(cscope_db):
    # Do a quick test if the cscope is already built
    if not os.path.exists("cscope.out"):
        os.system("cscope -bqR -f %s" % cscope_db)
        print ("Built Database with name %s" % cscope_db)

#
# Different Types of cscope queries
#

# need to correct this one 
def lookup_symbol_references(symbol):
    os.system("cscope -d -f %s -L0 %s > .%s_lookup" % (CSCOPE_DB,symbol,symbol) )
    file_handle = open(".%s_lookup" % symbol , 'r+')
    file_handle.readlines()

# input : symbol name 
# output: None or the string format with the cscope output
def lookup_symbol_definition(symbol):
    os.system("cscope -d -f %s -L1 %s > .%s_def" % (CSCOPE_DB,symbol,symbol) )
    file_handle = open(".%s_def" % symbol , "r+")
    try:
        line_zero = file_handle.readlines()[0]
        rc = line_zero
    except IndexError:
        rc = None
    file_handle.close()
    os.system("rm .%s_def" % symbol)
    return rc
#input : symbol name
#output: set of functions directly called by symbol
def functions_called_by_this_function(symbol):
    function_set = set(); # This will make sure there are one refernce from atleast that func
    logging.info("Looking for functions called by this function : %s" % symbol )
    os.system("cscope -d -f %s -L2 %s > .functions_called_by_%s" % (CSCOPE_DB,symbol,symbol) )
    file_handle = open(".functions_called_by_%s" % symbol , "r+")
    lines = file_handle.readlines()
   
    for each_cscope_line in lines:
        split_line = each_cscope_line.split()
        called_symbol_name = split_line[1]
        function_set.add(called_symbol_name)
   
    file_handle.close()
    os.system("rm .functions_called_by_%s" % symbol)
    return function_set
#input : symbol name
#output: set of functions directly called by symbol
def functions_called_by_this_function_raw_output(symbol):
    os.system("cscope -d -f %s -L2 %s > .functions_called_by_%s" % (CSCOPE_DB,symbol,symbol) )
    file_handle = open(".functions_called_by_%s" % symbol , "r+")
    lines = file_handle.readlines()
   
    file_handle.close()
    os.system("rm .functions_called_by_%s" % symbol)
    return lines 
#input : symbol name
#output: set of functions directly calling this symbol
def functions_calling_this_function(symbol):
    function_set = set(); # This will make sure there are one refernce from atleast that func
    logging.info("Looking for functions calling this function : %s" % symbol) 
    os.system("cscope -d -f %s -L3 %s > .functions_calling_%s" % (CSCOPE_DB,symbol,symbol) )
    file_handle = open(".functions_calling_%s" % symbol , "r+")
    lines = file_handle.readlines()
   
    for each_cscope_line in lines:
        split_line = each_cscope_line.split()
        called_symbol_name = split_line[1]
        function_set.add(called_symbol_name)
    
    file_handle.close()
    os.system("rm .functions_calling_%s" % symbol)
    return function_set
#input : symbol name
#output: set of functions directly calling this symbol
def functions_calling_this_function_raw_output(symbol):
    os.system("cscope -d -f %s -L3 %s > .functions_calling_%s" % (CSCOPE_DB,symbol,symbol) )
    file_handle = open(".functions_calling_%s" % symbol , "r+")
    lines = file_handle.readlines()
   
    
    file_handle.close()
    os.system("rm .functions_calling_%s" % symbol)
    return lines 
#input : starting symbols
#output: set of all the symbols going down recursively, also it will ignore functions in the ignorelise
def find_all_downstream_symbols(starting_symbol):
    # Create two sets already_processed and under_processing
    already_processed = set()
    to_be_processed = set()
    to_be_processed.add(starting_symbol) # Add the very first symbol

    while len(to_be_processed) > 0:  # that means set is not empty
        current_symbol_processing = to_be_processed.pop()
        # print "Processing current symbol : %s " % current_symbol_processing 
        if current_symbol_processing not in already_processed and not symbolInIgnoreList(current_symbol_processing):
            # Add to already processed set
            already_processed.add(current_symbol_processing)
           
            set_of_called_functions = functions_called_by_this_function(current_symbol_processing);
            to_be_processed = to_be_processed.union(set_of_called_functions)
    return already_processed


# @ This function given a starting symbol will figure out all the downstream
#   symbols
# @ Old function is gonna be deprecated in favour of this
# @ Level provides a way of controlling how deep down you want to go
# input : starting symbol , levels
#
#
def find_all_downstream_symbols_v2(starting_symbol, default_level=STACK_DEPTH):
    # Create the dictionary{level:(set, of, symbols)}

    level_listOfSymbols_dict = dict()
    level_listOfSymbols_dict[0] = {starting_symbol}
    current_level = 0;



    # now the heart of the function
    while current_level < default_level:
        # Lets check if we are done
        if len(level_listOfSymbols_dict[current_level]) == 0:
            break

        level_listOfSymbols_dict[current_level+1] = set() # create a set for the next level

        
        # For current level, check all the functions and iterate over them 
        for symbol in level_listOfSymbols_dict[current_level]:


            set_of_called_functions = functions_called_by_this_function(symbol)
        

            # iterate over all set to see if they are in ignored list
            updated_set_of_called_functions = set()
            for symbol in set_of_called_functions:
                if not symbolInIgnoreList(symbol):
                    updated_set_of_called_functions.add(symbol)

            # now after pruning the set , add it to the dictionary
            level_listOfSymbols_dict[current_level+1] = level_listOfSymbols_dict[current_level+1].union(updated_set_of_called_functions)

        # Move further downstream
        current_level = current_level + 1 
    
    
    # Convert it to logging
    # pp.pprint(level_listOfSymbols_dict)

    return level_listOfSymbols_dict


# @ This function given a starting symbol will figure out all the upstream
#   symbols
# @ Old function is gonna be deprecated in favour of this
# @ Level provides a way of controlling how deep down you want to go
# input : starting symbol , levels
#
#
def find_all_upstream_symbols_v2(starting_symbol, default_level=-3):
    # Create the dictionary{level:(set, of, symbols)}

    level_listOfSymbols_dict = dict()
    level_listOfSymbols_dict[0] = {starting_symbol}
    current_level = 0;



    # now the heart of the function
    while current_level > default_level:
        # Lets check if we are done
        if len(level_listOfSymbols_dict[current_level]) == 0:
            break

        level_listOfSymbols_dict[current_level-1] = set() # create a set for the next level

        
        # For current level, check all the functions and iterate over them 
        for symbol in level_listOfSymbols_dict[current_level]:

            set_of_calling_functions = functions_calling_this_function(symbol)
        

            # iterate over all set to see if they are in ignored list
            updated_set_of_calling_functions = set()
            for symbol in set_of_calling_functions:
                if not symbolInIgnoreList(symbol):
                    updated_set_of_calling_functions.add(symbol)
    
            # now after pruning the set , add it to the dictionary
            level_listOfSymbols_dict[current_level-1] = level_listOfSymbols_dict[current_level-1].union(updated_set_of_calling_functions)

        # Move further downstream
        current_level = current_level - 1 
    

    # Convert it to logging
    # pp.pprint(level_listOfSymbols_dict)

    return level_listOfSymbols_dict



#input : starting symbols
#output: set of all the symbols going up recursively, also it will ignore functions in the ignorelise
def find_all_upstream_symbols(starting_symbol):
    # Create two sets already_processed and under_processing
    already_processed = set()
    to_be_processed = set()
    to_be_processed.add(starting_symbol) # Add the very first symbol

    while len(to_be_processed) > 0:  # that means set is not empty
        current_symbol_processing = to_be_processed.pop()
        # print "Processing current symbol : %s " % current_symbol_processing 
        if current_symbol_processing not in already_processed and not symbolInIgnoreList(current_symbol_processing):
            # Add to already processed set
            already_processed.add(current_symbol_processing)
           
            set_of_calling_functions = functions_calling_this_function(current_symbol_processing);
            to_be_processed = to_be_processed.union(set_of_calling_functions)
    return already_processed

#input : symbol
#output: True if symbol is in ignore list
def symbolInIgnoreList(symbol):
    # Regex Use
    found = False
    for regex in function_ignore_set:
        # Check for gnu library calls
        if regex == "gnu-library-calls":
            for gcc_function in gnu_function_ignore_set:
                if symbol == gcc_function:
                    found=True
                    break

        searchObj = re.search(regex, symbol, re.M)
        if searchObj is not None:
            logging.debug("%s symbol will not be displayed" % symbol)
            found = True
            break
    return found

# input : set of functions , which we would get from find_all_[down/up]stream symbols
# output : edges between those set of functions
def list_of_edges_between_functions(list_of_functions):
    # print (list_of_functions)
    list_of_edges = list();
    # iterate over all the functions in list_of_functions
    for symbol_name in list_of_functions:
        
        # Print the definition of the symbol
        symbol_def = lookup_symbol_definition(symbol_name)
        if symbol_def == None: # that means this symbol is not there in the database and hence there will be no upstream and no downstream
            continue
        split_line = symbol_def.split()
        # print ("%-40s /* Definition Occurs at [%s +%s] */" % ( split_line[1], split_line[0], split_line[2]) )
        list_of_edges.append("%-40s /* Definition Occurs at [%s +%s] */" % ( split_line[1], split_line[0], split_line[2]) )

        # Step 2
        # Print the caller->callee relationship from downstream perspective
        func_list = functions_called_by_this_function(symbol_name)
        cscope_lines = functions_called_by_this_function_raw_output(symbol_name)
        for line in cscope_lines:
            newline = line.split()
            cs_file, cs_symbol, cs_lineno = newline[0], newline[1], newline[2]
            if cs_symbol in list_of_functions and not symbolInIgnoreList(cs_symbol):
                # print ("%s->%s \t\t/* Down : Occurs at[%s +%s] */" % (symbol_name , cs_symbol , cs_file, cs_lineno))
                list_of_edges.append("%s->%s \t\t/* Down : Occurs at[%s +%s] */" % (symbol_name , cs_symbol , cs_file, cs_lineno))
        # Step 3
        # Print the caller->callee relationship from upstream dir perspective
        func_list = functions_calling_this_function(symbol_name)
        cscope_lines = functions_calling_this_function_raw_output(symbol_name) 
        for line in cscope_lines:
            newline = line.split()
            cs_file, cs_symbol, cs_lineno = newline[0], newline[1], newline[2]
            if cs_symbol in list_of_functions and not symbolInIgnoreList(cs_symbol):
                # print ("%s->%s \t\t/* Up : Occurs at[%s +%s] */" % (cs_symbol , symbol_name , cs_file, cs_lineno))
                list_of_edges.append("%s->%s \t\t/* Up : Occurs at[%s +%s] */" % (cs_symbol , symbol_name , cs_file, cs_lineno))
    # Return the list of all the edges needed for dot program
    return list_of_edges


# @ This function creates the edges between functions
# @ input is expected in the form {leve, {set of function}} dict format
#
#
#

def list_of_edges_between_functions_v2(level_listOfSymbols_dict):
    # pp.pprint(level_listOfSymbols_dict)

    list_of_edges = list();
    # iterate over all the levels and make a bigger set
    set_of_functions = set()
    for level in level_listOfSymbols_dict.keys():
        if len(level_listOfSymbols_dict[level]) == 0:
            continue
        set_of_functions = set_of_functions.union(level_listOfSymbols_dict[level])
    # pp.pprint(set_of_functions)



    # iterate over all the functions in set of the functions
    for symbol_name in set_of_functions:
        
        # Step 1 :Print the definition of the symbol
        symbol_def = lookup_symbol_definition(symbol_name)
        if symbol_def == None: # that means this symbol is not there in the database and hence there will be no upstream and no downstream
            continue
        split_line = symbol_def.split()
        # print ("%-40s /* Definition Occurs at [%s +%s] */" % ( split_line[1], split_line[0], split_line[2]) )
        list_of_edges.append("%-40s /* Definition Occurs at [%s +%s] */" % ( split_line[1], split_line[0], split_line[2]) )

        # Step 2
        # Print the caller->callee relationship from downstream perspective
        func_list = functions_called_by_this_function(symbol_name)
        cscope_lines = functions_called_by_this_function_raw_output(symbol_name)
        for line in cscope_lines:
            newline = line.split()
            cs_file, cs_symbol, cs_lineno = newline[0], newline[1], newline[2]
            if cs_symbol in set_of_functions and not symbolInIgnoreList(cs_symbol):
                # print ("%s->%s \t\t/* Down : Occurs at[%s +%s] */" % (symbol_name , cs_symbol , cs_file, cs_lineno))
                list_of_edges.append("%s->%s \t\t/* Down : Occurs at[%s +%s] */" % (symbol_name , cs_symbol , cs_file, cs_lineno))

        # Step 3
        # Print the caller->callee relationship from upstream dir perspective
        func_list = functions_calling_this_function(symbol_name)
        cscope_lines = functions_calling_this_function_raw_output(symbol_name) 
        for line in cscope_lines:
            newline = line.split()
            cs_file, cs_symbol, cs_lineno = newline[0], newline[1], newline[2]
            if cs_symbol in set_of_functions and not symbolInIgnoreList(cs_symbol):
                # print ("%s->%s \t\t/* Up : Occurs at[%s +%s] */" % (cs_symbol , symbol_name , cs_file, cs_lineno))
                list_of_edges.append("%s->%s \t\t/* Up : Occurs at[%s +%s] */" % (cs_symbol , symbol_name , cs_file, cs_lineno))
    # Return the list of all the edges needed for dot program
    # pp.pprint(list_of_edges)
    return list_of_edges




def filter_graph_edges_based_on_direction(list_of_edges, dir="Down"):
    # print (list_of_edges)
    new_list_of_edges = list()
    for edge in list_of_edges:
        if dir in edge.split():
            new_list_of_edges.append(edge)
    return new_list_of_edges


def get_symbol_ignorelist():
    # open ignore symbol list
    file_handle = open(".ignorefunctionlist") 
    lines = file_handle.readlines()
    logging.debug("Following Functions will be ignored : ")
    for line in lines:
        line = line.rstrip()
        function_ignore_set.add(line)

    # gnu library call llist
    file_handle = open("gnu-library-calls.txt")
    lines = file_handle.readlines()
    for line in lines:
        line = line.rstrip()
        gnu_function_ignore_set.add(line)

# Need to change the logic , right now it wont work for some functions
# input list of edges, and regex_expression of the nodes that you want to color
# note: if multiple regexes , then multiple times.
# TODO: expose color in the API
def color_nodes_in_graph_lhs(edge_list, color='red'):
    
    for edge in edge_list:
        # print (edge)
        searchObj = re.search(r'.*->.* ', edge)
        if searchObj is not None:
            # found the graph line, now get the lhs
            lhs = edge.split()[0]
            edge_list.append("%s [color=%s]" % (lhs, color))
        '''
        searchObj = re.search(regex_expression , edge , re.M)
        if searchObj is not None:
            #found a edge which is a definition
            symbol_name = edge.split()[0]
            if symbol_name in symbol_list_to_be_colored:
                edge_list.append("%s [color=red]" % symbol_name)
        '''
    return edge_list #updated edge list

def color_nodes_in_graph_rhs(edge_list, color='blue'):
    
    for edge in edge_list:
        # print (edge)
        searchObj = re.search(r'.*->.* ', edge)
        if searchObj is not None:
            # found the graph line, now get the lhs
            rhs = edge.split()[2]
            edge_list.append("%s [color=%s]" % (rhs, color))
        '''
        searchObj = re.search(regex_expression , edge , re.M)
        if searchObj is not None:
            #found a edge which is a definition
            symbol_name = edge.split()[0]
            if symbol_name in symbol_list_to_be_colored:
                edge_list.append("%s [color=red]" % symbol_name)
        '''
    return edge_list #updated edge list

def create_graph_database(edge_list,print_to_file=False,filename="output.dot"):
    file_handle = open(filename, "w+")

    if print_to_file:
        file_handle.write("digraph symbol_tree {\ngraph [rankdir=LR, concentrate=true];\nnode [shape=record];\nedge [];\n")
    else:
        print("digraph symbol_tree {\ngraph [rankdir=LR, concentrate=true];\nnode [shape=record];\nedge [];\n")

    for edge in edge_list:
        if print_to_file:
            file_handle.write("\t" + edge + "\n")
        else:
            print ("\t" + edge)


    if print_to_file:
        file_handle.write("}" +"\n")
    else:
        print("}")
    file_handle.close()

#inout : takes two symbols
#output: returns the set of the functions which intersect
def subgraph_intersected_functions(down_symbol, up_symbol): # downstream: functions called by down_symbol
    set_of_down_symbols = find_all_downstream_symbols(down_symbol) 
    set_of_up_symbols = find_all_upstream_symbols(up_symbol)
    intersected_set = set_of_down_symbols.intersection(set_of_up_symbols)
    return intersected_set

#input : takes any number of symbols in form of a list
#output: returns the set of the functions which intersect
def subgraph_intersected_functions_infinite(list_of_symbols):
    intersected_list = list()


    # since this is time consuming operation, need to show the status bar
    # for progress

    symbol_list_len = len(list_of_symbols)
    maxval = symbol_list_len * symbol_list_len
    current_val = 0
    bar = progressbar.ProgressBar(maxval, \
        widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    bar.start()
    # end of part related to progress bar

    for down_symbol in list_of_symbols:
        for up_symbol in list_of_symbols:
            if (down_symbol != up_symbol):
                returned_intersected_set = subgraph_intersected_functions(down_symbol,up_symbol)
                intersected_list = intersected_list + list(returned_intersected_set)
            bar.update(current_val)
            current_val = current_val + 1
    intersected_set = set(intersected_list)
    # related to display of progress bar
    bar.finish()
    #
    return intersected_set


#TODO : do the del children node optimization for the upstream direction too
# recursive call for interactive graph
def interactive_function_graph_down(symbol):
    # check for ignore list
    if symbolInIgnoreList(symbol):
        return

    dict_for_node = dict() ; # have three keys 1:name , 2: size , 3: childre(if not a leaf node)
    dict_for_node["name"] = str(symbol);
    dict_for_node["size"] = 10 # irrelevant
    dict_for_node["children"] = list()
    imm_down_symbols = functions_called_by_this_function(symbol)
    # also make sure that we dont forever recurse
    if len(imm_down_symbols) == 0:   # which means that this is leaf node
        if 'children' in dict_for_node.keys():
            del dict_for_node['children']
        # print (dict_for_node)
        return dict_for_node        
    else:
        for function in imm_down_symbols:
            if function != symbol:
                returned_value = interactive_function_graph_down(function)
                if returned_value != None:
                    dict_for_node["children"].append(returned_value)
        # print (dict_for_node)
        if dict_for_node['children'] == []: # that means empty list
            del dict_for_node['children']
        return (dict_for_node)

def interactive_function_graph_up(symbol):
    dict_for_node = dict() ; # have three keys 1:name , 2: size , 3: childre(if not a leaf node)
    dict_for_node["name"] = str(symbol);
    dict_for_node["size"] = 10 # irrelevant
    dict_for_node["children"] = list()
    imm_down_symbols = functions_calling_this_function(symbol)
    # also make sure that we dont forever recurse
    if len(imm_down_symbols) == 0:   # which means that this is leaf node
        print (dict_for_node)
        return dict_for_node        
    else:
        for function in imm_down_symbols:
            if function != symbol:
                dict_for_node["children"].append(interactive_function_graph_up(function))
        print (dict_for_node)
        return (dict_for_node)

# interactive graph generation for subgraph(end node will be repeated multiple times)
def interactive_function_subgraph(start_symbol , intersected_function_list):
    dict_for_node = dict() ; # have three keys 1:name , 2: size , 3: childre(if not a leaf node)
    dict_for_node["name"] = str(start_symbol);
    # print ("Print after name assigned and start symbol = %s" %start_symbol , dict_for_node);
    dict_for_node["size"] = 10 # irrelevant
    dict_for_node["children"] = list()
    imm_down_symbols = functions_called_by_this_function(start_symbol)
    # also make sure that we dont forever recurse
    if len(imm_down_symbols) == 0:   # which means that this is leaf node
        # print (dict_for_node)
        return dict_for_node
    else:
        for function in imm_down_symbols:
            if function != start_symbol and function in intersected_function_list:
                dict_for_node["children"].append(interactive_function_subgraph(function,intersected_function_list))
        print (dict_for_node)
        return (dict_for_node)

#TODO function to use the dot program to convert to png and esp web file
#input  : filename.c or full file name (/a/b/c/filename.c)
#output : _gen_filename directory
def generate_up_and_down_graph_for_complete_file(filename):
    base_filename = os.path.basename(filename)
    print ( '*' * 10 + 'Generating up and down graphs for file %s' % base_filename +'*' * 10)
    # First search for the filename 
    dir_name = '_gen_' + base_filename.split('.')[0]
    print( 'The name of generate folder is %s' % dir_name)
    # Before creating the dictionary , look if the file exists and what is the file path
    os.system("cscope -d -f %s -L7  %s > .checkfilename" % (CSCOPE_DB, filename))
    file_handle = open(".checkfilename" , "r+")
    lines = file_handle.readlines() ; file_handle.close()
    print (lines)

    # this is assuming it is not empty
    full_path = lines[0].split()[0]
    print ("Full path = " + full_path)

    # Create the directory in the same folder where the file exists
    file_path_wo_end_file = full_path.split('/')
    file_path_wo_end_file = '/'.join(file_path_wo_end_file[:-1])
    print (file_path_wo_end_file)

    os.system('mkdir -p _gen_dir/%s/%s' % (file_path_wo_end_file,dir_name))
    
    # Get the list of the functions in that file
    os.system('ctags -x --c-kinds=fp %s > .functionlist ' % full_path)
    file_handle = open(".functionlist" , "r+")
    lines = file_handle.readlines() ; file_handle.close()

    # print (lines)
    # Create a set of functions
    function_set = list()
    for line in lines:
        func = line.split()[0]
        function_set.append(func)

    # now start generating the up and down file graphs
    for function in function_set:
        print ("Name of the function is " + function)
        create_down_graph(function)
        create_up_graph(function)

    # now move them to generated directory
    os.system('mv *_down*.html *_down*.png *_down*.dot *_down*.map _gen_dir/%s/%s' % (file_path_wo_end_file,dir_name))
    os.system('mv *_up*.html *_up*.png *_up*.dot *_up*.map _gen_dir/%s/%s' % (file_path_wo_end_file,dir_name))




def create_down_graph(function):
    function_name = function 
    all_downstream_symbols = find_all_downstream_symbols(function_name)
    list_of_edges = list_of_edges_between_functions(all_downstream_symbols)
    list_of_edges = filter_graph_edges_based_on_direction(list_of_edges, dir="Down")
    create_graph_database(list_of_edges,print_to_file=True,filename="%s_down.dot" % function_name)
    os.system('dot -Tpng -o %s_down.png -Tcmapx -o %s_down.map %s_down.dot' % (function_name, function_name, function_name))
    os.system('(echo "<IMG SRC="%s_down.png" USEMAP="#symboltree" />"; cat %s_down.map) >  %s_down.html' % (function_name, function_name, function_name))



# @ Updated version of create_down_graph , no changes since data structure to this function remains same
def create_down_graph_v2(function, stack_depth=STACK_DEPTH):
    function_name = function
    all_downstream_symbols = find_all_downstream_symbols_v2(function_name, stack_depth)
    list_of_edges = list_of_edges_between_functions_v2(all_downstream_symbols)
    list_of_edges = filter_graph_edges_based_on_direction(list_of_edges, dir="Down")
    create_graph_database(list_of_edges,print_to_file=True,filename="%s_down.dot" % function_name)
    os.system('dot -Tpng -o %s_down.png -Tcmapx -o %s_down.map %s_down.dot' % (function_name, function_name, function_name))
    os.system('(echo "<IMG SRC="%s_down.png" USEMAP="#symboltree" />"; cat %s_down.map) >  %s_down.html' % (function_name, function_name, function_name))
    # pp.pprint(list_of_edges)

def create_up_graph(function):
    function_name = function 
    all_upstream_symbols = find_all_upstream_symbols(function_name)
    list_of_edges = list_of_edges_between_functions(all_upstream_symbols)
    list_of_edges = filter_graph_edges_based_on_direction(list_of_edges, dir="Up")
    create_graph_database(list_of_edges,print_to_file=True,filename="%s_up.dot" % function_name)
    os.system('dot -Tpng -o %s_up.png -Tcmapx -o %s_up.map %s_up.dot' % (function_name, function_name, function_name))
    os.system('(echo "<IMG SRC="%s_up.png" USEMAP="#symboltree" />"; cat %s_up.map) >  %s_up.html' % (function_name, function_name, function_name))

# @ Updated version of create_down graph , no change than previous version
def create_up_graph_v2(function, stack_depth=-(STACK_DEPTH)):
    function_name = function 
    all_upstream_symbols = find_all_upstream_symbols_v2(function_name, stack_depth)
    # pp.pprint(all_upstream_symbols)
    list_of_edges = list_of_edges_between_functions_v2(all_upstream_symbols)
    # pp.pprint(list_of_edges)
    list_of_edges = filter_graph_edges_based_on_direction(list_of_edges, dir="Up")
    create_graph_database(list_of_edges,print_to_file=True,filename="%s_up.dot" % function_name)
    os.system('dot -Tpng -o %s_up.png -Tcmapx -o %s_up.map %s_up.dot' % (function_name, function_name, function_name))
    os.system('(echo "<IMG SRC="%s_up.png" USEMAP="#symboltree" />"; cat %s_up.map) >  %s_up.html' % (function_name, function_name, function_name))

# Following code is related to figuring out the component related code
# Component will be based on the list of the files provided
def get_list_of_functions_in_a_component(list_of_files):
   # Get all the functions getting listed in the files(ctags) and get a set of it
   set_of_functions_in_component = set()
   set_of_immediate_callers_for_component = set()
   set_of_immediate_callings_for_component = set()
   for filename in list_of_files:
        # print (filename)
        process_that_file_and_functions_in_set(set_of_functions_in_component,filename)
   return set_of_functions_in_component


def process_that_file_and_functions_in_set(set_of_functions_in_component,filename):
    os.system('ctags -x --c-kinds=fp %s > temp_output' % filename)
    file_handle = open("temp_output" , "r+")
    lines = file_handle.readlines() ; file_handle.close()

    # parse the info from the file
    for line in lines:
        func_name = line.split()[0]
        if not symbolInIgnoreList(func_name):
            set_of_functions_in_component.add(func_name)


#input  : component which is input to the set of files
#output : output will be {filename : functionlist} dictionary format
#         TODO : make it graphical which should be easy but do it after some time 
def get_external_interface_to_the_component(component, component_name='component_inbound'):
    # for filename in component:
    #     print (filename)
    list_of_funcs = get_list_of_functions_in_a_component(component)
    print (list_of_funcs)
    # once we have the list of the functions in the component, then the immediate
    # {file, functios = [] } key = file and value = list of the the functions in
    # that file
    file_functions_dict = dict()
    ext_func_2_comp_func_dict = dict() # external function calling component function
    # iterate over all the functions and check their immediate predecessors
    for func in list_of_funcs:
        print ('*' * 10 , 'Function name is ' , func)
        raw_lines = functions_calling_this_function_raw_output(func)
        for line in raw_lines:
            splitted_line = line.split()
            # print (splitted_line)
            calling_function = splitted_line[1]

            
            if symbolInIgnoreList(calling_function):
                continue

            print ("calling function = " , calling_function)
            calling_function_file = splitted_line[0]

            # Dont want to see .h files
            if re.search('.*.h$' , calling_function_file, re.M) != None:
                continue

            if calling_function_file not in file_functions_dict.keys():
                file_functions_dict[calling_function_file] = list()


            if calling_function not in ext_func_2_comp_func_dict.keys():
                ext_func_2_comp_func_dict[calling_function] = list()

            # if the calling functions is part of the same set , then its not external interface
            if not calling_function in list_of_funcs:
                ext_func_2_comp_func_dict[calling_function].append(func)
                print (calling_function, calling_function_file)
                if calling_function not in file_functions_dict[calling_function_file]:
                    file_functions_dict[calling_function_file].append(calling_function)

    print (json.dumps(file_functions_dict, indent=2))
    print (json.dumps(ext_func_2_comp_func_dict, indent=2))
    print (file_functions_dict.keys())


    # now since this is related to component code, not putting this in seperate func

    # start looking at the file_functs dict and create a graph out out of it
    list_of_edges1 = []
    for filename in file_functions_dict.keys():
        print set(file_functions_dict[filename])
        for function_name in file_functions_dict[filename]:
            list_of_edges1.append("%s -> %s" % ( '_'.join(re.split('/|\.|\-',filename)) , function_name))

    # color nodes of filenames
    list_of_edges1 = color_nodes_in_graph_lhs(list_of_edges1 ,'red' )
    list_of_edges1 = color_nodes_in_graph_rhs(list_of_edges1 ,'blue' )

    list_of_edges2 = []
    for external_function in ext_func_2_comp_func_dict.keys():
        for component_func in ext_func_2_comp_func_dict[external_function]:
            list_of_edges2.append("%s -> %s" % ( '_'.join(re.split('/|\.',external_function)) , component_func))

    list_of_edges2 = color_nodes_in_graph_rhs(list_of_edges2 ,'black' )
    print (list_of_edges2)

    list_of_edges = list_of_edges1 + list_of_edges2
    

    # Create the database
    create_graph_database(list_of_edges , print_to_file=True, filename='comp_%s_down.dot' % component_name )

    os.system('dot -Tpng -o comp_%s_down.png -Tcmapx -o comp_%s_down.map comp_%s_down.dot' % (component_name, component_name, component_name))
    os.system('(echo "<IMG SRC="comp_%s_down.png" USEMAP="#symboltree" />"; cat comp_%s_down.map) >  comp_%s_down.html' % (component_name, component_name, component_name))

#input  : component which is input to the set of files
#output : output will be {filename : functionlist} dictionary format
#         TODO : make it graphical which should be easy but do it after some time 
def get_external_interface_from_the_component(component, component_name='component_outbound'):
    # for filename in component:
    #     print (filename)
    list_of_funcs = get_list_of_functions_in_a_component(component)
    print (list_of_funcs)
    # once we have the list of the functions in the component, then the immediate
    # {file, functios = [] } key = file and value = list of the the functions in
    # that file
    file_functions_dict = dict()
    comp_func_2_ext_func_dict = dict() # external function calling component function
    # iterate over all the functions and check their immediate predecessors
    for func in list_of_funcs:
        print ('*' * 10 , 'Function name is ' , func)
        raw_lines = functions_called_by_this_function_raw_output(func)
        for line in raw_lines:
            splitted_line = line.split()
            print (splitted_line)
            called_function = splitted_line[1]

            if symbolInIgnoreList(called_function):
                continue

            if lookup_symbol_definition(called_function) is  None:
                continue

            called_function_file = lookup_symbol_definition(called_function).split()[0]
            if called_function not in file_functions_dict.keys():
                file_functions_dict[called_function] = list()

            print ("called function = " , called_function)
            print ('called function def = ' , called_function_file)


            if func not in comp_func_2_ext_func_dict.keys():
                comp_func_2_ext_func_dict[func] = list()

            # if the called functions is part of the same set , then its not external interface
            if not called_function in list_of_funcs:
                comp_func_2_ext_func_dict[func].append(called_function)
                print (comp_func_2_ext_func_dict[func])
                print (called_function, called_function_file)
                if called_function_file not in file_functions_dict[called_function]:
                    file_functions_dict[called_function].append(called_function_file)

    print (json.dumps(file_functions_dict, indent=2))
    print (json.dumps(comp_func_2_ext_func_dict, indent=2))
    print (file_functions_dict.keys())


    # now since this is related to component code, not putting this in seperate func

    # start looking at the file_functs dict and create a graph out out of it
    list_of_edges1 = []
    for function_name in file_functions_dict.keys():
        # print set(file_functions_dict[called_function])
        for filename in file_functions_dict[function_name]:
            list_of_edges1.append("%s -> %s" % ( function_name, '_'.join(re.split('/|\.',filename))))

    # color nodes of filenames
    list_of_edges1 = color_nodes_in_graph_lhs(list_of_edges1 ,'blue' )
    list_of_edges1 = color_nodes_in_graph_rhs(list_of_edges1 ,'red' )

    list_of_edges2 = []
    for component_func in comp_func_2_ext_func_dict.keys():
        for external_function in comp_func_2_ext_func_dict[component_func]:
            list_of_edges2.append("%s -> %s" % ( '_'.join(re.split('/|\.',component_func)) , external_function))

    list_of_edges2 = color_nodes_in_graph_lhs(list_of_edges2 ,'black' )
    # print (list_of_edges2)

    list_of_edges = list_of_edges1 + list_of_edges2
    

    # Create the database
    create_graph_database(list_of_edges , print_to_file=True, filename='comp_%s_down.dot' % component_name )

    os.system('dot -Tpng -o comp_%s_down.png -Tcmapx -o comp_%s_down.map comp_%s_down.dot' % (component_name, component_name, component_name))
    os.system('(echo "<IMG SRC="comp_%s_down.png" USEMAP="#symboltree" />"; cat comp_%s_down.map) >  comp_%s_down.html' % (component_name, component_name, component_name))
    

# input : dir_name is the name od the dir , use_regex for developers only, to filter the file list
def create_inbound_component_interface_for_dir(dir_name, use_regex=False, avoid_regex=''):
    print ( 'Directory Name = ', dir_name)
    file_list = os.listdir(dir_name)

    # Update the file list based on the regex for giving it for further processing
    if use_regex:
        final_file_list = []
        for file_name in file_list:
            compileOjb = re.compile(avoid_regex)
            if compileOjb.match(file_name) != None:
                continue
            else:
                final_file_list.append(dir_name + '/' + file_name)
    else:
        final_file_list  = file_list
    print ("The final file list is " , final_file_list)
    os.system('sleep 10')

    get_external_interface_to_the_component( final_file_list )

################################################################################
# Invocation
if __name__=='__main__':


    # create a pretty printer
    pp = pprint.PrettyPrinter()



    # Start Logger 
    logging.basicConfig(filename="graph.log" ,level=logging.DEBUG)

    # For later to color the output during the build
    get_symbol_ignorelist()
    
    # Build or check for existing database
    CSCOPE_DB = set_the_name_of_cscope_db()
    # print ("Building database")
    build_cscope_db(CSCOPE_DB)
    # print ("Database Done")


    '''
    # This is the test for interactive graph 
    # output_dict = interactive_function_graph_up('qos_uninstall_port_policy') 
    output_dict = interactive_function_graph_down('rrcReAdmissionDecision')
    print (json.dumps(output_dict, indent=2))
    '''
    '''
    # test for interactive graph 2 
    func1 = "updateRcbRadioPolicyNameState"
    func2 = "__attribute__"
    intersected_functions = subgraph_intersected_functions(func1, func2)
    print ( intersected_functions )
    output_dict = interactive_function_subgraph(func1, intersected_functions)
    print (json.dumps(output_dict))
    '''

 




    parser = argparse.ArgumentParser("Description of the SCAV program")
    parser.add_argument('-a','--action',help='Provide action that you want to do:DownGraph, UpGraph, SubGraph, Sub_Many_Graph, component_inbound, component_outbound, dir_component')
    parser.add_argument('-d','--direction',help='Up/Down')
    parser.add_argument('-f','--single_function',help='Please Single Function Name')
    parser.add_argument('-l','--list_of_functions',help='Quoted List of Function Names')
    # parser.add_argument('-i','--ignorefunctionlist',help='Put functions in .ignorelist to prevent them from popping up in generated graph')
    parser.add_argument('-i','--inode_name',help='Name of the file and it will generate graphs for all the functions in that file..!! may take some time')
    parser.add_argument('-c','--component', help='Provide the list of the files which you consider as a component', nargs='+')
    parser.add_argument('-b','--block_of_files', help='Provide the dir files which you consider as a component and in that case -c components files will be ignored')
    parser.add_argument('-v', '--version' , help="select between 1 and 2")
    parser.add_argument('-n', '--nth' , help="nth level of call graph, for DOWN graph value > 0 and for UP < 0")

    # Parse all the arguments
    args = vars(parser.parse_args())






    # Check if deep call stack has been defined
    deep = int ( args['nth'] if args['nth'] else 3 )

    ###################################
    # All related checks are put here #
    ###################################

    # Usage for component interface and component graph using -b option in which we just provide the directory name
    if args['action'] == 'dir_component':
        if args['block_of_files'] == None:
            print("Provide the name of the directory using -b option")
            exit()
        else:
            dir_name = args['block_of_files']
            # create_inbound_component_interface_for_dir(dir_name, use_regex = True, avoid_regex = '.*.h$')
            create_inbound_component_interface_for_dir(dir_name, use_regex = True, avoid_regex = '^dev_.*.c|^.CC|.*errmsg.*.c$|.*.h$|.*Makefile.*|.*Kconfig.*')




    # Check Code : Dont want to push into the actual implementation to 
    # keep it readable , since we are all engineers , we dont need error detection
    # code.

    #  new interface to downward symbol
    if args['version'] == '2':
        # keep the interface same , but call version 2 
        if args['action'] == 'DownGraph':
            # look for function name
            if args['single_function'] == None:
                print ("Please provide the name of the function")
                exit()
            else:
                create_down_graph_v2(args['single_function'] , deep)
            exit() # this is where the test stops

    if args['version'] == '2':
        # keep the interface same , but call version 2 
        if args['action'] == 'UpGraph':
            # look for function name
            if args['single_function'] == None:
                print ("Please provide the name of the function")
                exit()
            else:
                create_up_graph_v2(args['single_function'] , -(deep))
            exit() # this is where the test stops

    # Checks for Downgraph TODO: add version check
    if args['action'] == 'DownGraph':
        # look for function name
        if args['single_function'] == None:
            print ("Please provide the name of the function")
            exit()
        else:
            create_down_graph(args['single_function'])
        exit() # this is where the test stops


    # Checks for UpGraph
    if args['action'] == 'UpGraph':
        # look for function name
        if args['single_function'] == None:
            print ("Please provide the name of the function")
            exit()
        else:
            create_up_graph(args['single_function'])
        exit() # this is where the test stops

    #TODO : Pull out graph_generation part
    # Checks for Subgraph
    if args['action'] == 'SubGraph':
        # look for function names
        if args['list_of_functions'] == None:
            print ("Please provide the name of 2 functions")
            exit()
        else:
            function_name_list = args['list_of_functions']
            func1 , func2 = function_name_list.split()[0]  , function_name_list.split()[1]
            print ("Func 1 : %s Func 2 : %s" % (func1, func2))
        funcs = subgraph_intersected_functions(func1, func2)
        list_of_edges= list_of_edges_between_functions(funcs);
        list_of_edges = filter_graph_edges_based_on_direction(list_of_edges, dir="Down")
        # list_of_edges = color_nodes_in_graph(list_of_edges, ["updateRscbPolicyNameState","CHECK_NULL_AND_FREE"])
        create_graph_database(list_of_edges,print_to_file=True,filename="%s_%s.dot" % (func1,func2))
        os.system('dot -Tpng -o %s_%s.png -Tcmapx -o %s_%s.map %s_%s.dot' % (func1, func2, func1,func2,func1,func2))
        os.system('(echo "<IMG SRC="%s_%s.png" USEMAP="#symboltree" />"; cat %s_%s.map) >  %s_%s.html' % (func1, func2, func1, func2,func1,func2))
        exit() # this is where the test stops

    # TODO: Sub_graph_infinite test to do and put it here, its more 
    #       useful for generating the graph of a component
    # Test 4: Write a test about it and think how to create a better understanding graph
    # This has been postponed since its tough to give a list of functions names on the command lien
    # and hence we can go into the file and edit the code
    # if args['action'] == 'Sub_Many_Graph':

    # Complete File Generation Usage and Checks: 
    # Take a name of the file and genereate a dir with the file_name and put all the up and down functions in that list
    if args['inode_name'] != None:
        file_name = args['inode_name']
        generate_up_and_down_graph_for_complete_file(file_name)


    ###### component related tests
    # TODO: 1. Make this is a seperate functions
    #      2. Get a better graph processing function (ahh!! .. should have taken up Graph Theory rathen than Computer Networks)
    #      3. Also figure out how to make component graph smaller
    '''
    # test 1
    file_list = ['qos/src/qos_downstream.c' , 'qos/src/qos_upstream.c']
    func1='donwstream' ; func2='upstrema'
    list_of_funcs = get_list_of_functions_in_a_component(file_list)
    print (list_of_funcs)
    # funcs = subgraph_intersected_functions_infinite(list_of_funcs)
    # file_handle=open('something' , 'r+')
    # lines = file_handle.readlines()
    # funcs  = ast.literal_eval(lines[2])
    # print (funcs)
    funcs = subgraph_intersected_functions_infinite(list_of_funcs)
    list_of_edges= list_of_edges_between_functions(funcs);
    list_of_edges_down = filter_graph_edges_based_on_direction(list_of_edges, dir="Down")
    # print (list_of_edges)
    list_of_edges_up = filter_graph_edges_based_on_direction(list_of_edges, dir="Up")
    list_of_edges = list_of_edges_up  + list_of_edges_down
    # list_of_edges = color_nodes_in_graph(list_of_edges, ["updateRscbPolicyNameState","CHECK_NULL_AND_FREE"])
    create_graph_database(list_of_edges,print_to_file=True,filename="%s_%s.dot" % (func1,func2))
    os.system('dot -Tpng -o %s_%s.png -Tcmapx -o %s_%s.map %s_%s.dot' % (func1, func2, func1,func2,func1,func2))
    os.system('(echo "<IMG SRC="%s_%s.png" USEMAP="#symboltree" />"; cat %s_%s.map) >  %s_%s.html' % (func1, func2, func1, func2,func1,func2))
    exit() # this is where the test stops
    '''


    # Test 2 (external interface to the component)
    # external interface meaning , all the immediate functions calling any function
    # in the interface.
    # no graph will  be generated for it, only text file 
    # TODO: Make it a function and perform some check to see if the output if correct
    # Do the same thing for outbound interface from a component
    if args['action'] == 'component_inbound':
        if args['component'] == None:
            print( 'Enter names of the files you consider as a component')
        else:
            component = args['component']
            get_external_interface_to_the_component(component)

    # Test 3: External interface from a component
    if args['action'] == 'component_outbound':
        if args['component'] == None:
            print( 'Enter names of the files you consider as a component')
        else:
            component = args['component']
            get_external_interface_from_the_component(component)

