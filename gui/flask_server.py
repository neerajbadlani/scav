from flask import Flask, render_template, request, redirect
import os
app = Flask(__name__)
@app.route("/hello")
def hello():
    print request.args['x']
    return "Hello World!"
from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/')
def hello_world():
#    <listen to calls from index.html>
#    <parse the url, get functionality>
#    <This will return us new html file, once its ready continue>
    
    return render_template('index.html')

@app.route("/search")
def parse():
    print request.args['q']
    print request.args['type']
    print request.args['r']
    func1 = request.args['r']
    func2 = request.args['q']
    action = request.args['type']
    
    #os.system("echo $PWD")
    if action.lower() == 'downstream':
        os.system("cd ..;python3 function_call_tree.py --action DownGraph -f %s; cp %s.* gui/templates/" %(func1,func1))
        image_link=os.getcwd()+'/templates/%s.html'%func1
    if action.lower() == 'upstream':
        os.system("cd ..;python3 function_call_tree.py --action UpGraph -f %s; cp %s.* gui/templates/" %(func1,func1))
        image_link=os.getcwd()+'/templates/%s.html'%func1
    #os.system("echo $PWD")
    if action.lower() == 'subgraph':
        os.system("cd ..;python3 function_call_tree.py --action SubGraph -l '%s %s' ; cp %s_%s.* gui/templates/" %(func1,func2,func1,func2))
        image_link=os.getcwd()+'/templates/%s_%s.html'%(func1,func2)

    print image_link
    #os.system("cd templates; echo '<a href=%s>Downstream homepage</a>'>image.html" %image_link)
    #return render_template("../static/handlePolicyInstallResponseForPort.html") 
    #return redirect("file:///nobackup/sumradha/hackathon/wcm/gate/lvl7dev/src/application/gui/templates/handlePolicyInstallResponseForPort.html",code=302)
    os.system("/usr/bin/firefox %s"%image_link)
  
if __name__ == '__main__':
     app.run()

