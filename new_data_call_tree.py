import os
import re
import pprint
import json
import progressbar
from time import sleep
import logging
import argparse





# filename_h : complete path from the root

# input : filename_h - full path from root (\) and Include path for CPP to work with
# output: to figure out 
def parse_h_file_and_give_data_strcuture(filename_h,I_path='.',defines=''):
    

    base_filename = os.path.basename(filename_h)
    logging.info("Parsing file : Filename give = %s | Base Filename = %s\ninclude_path = %s" % (filename_h , base_filename,I_path))

    # generate combined include path in case more than one include paths are given
    generated_I_string = ''
    for path in I_path:
        generated_I_string = generated_I_string + '  -I' + path;
    # generate defines to be passed to the program
    generated_D_string = ''
    for define in defines:
        generated_D_string = generated_D_string + ' -D' + define
    print (generated_D_string)
    print (generated_I_string)

    if os.system('cpp %s %s %s>& _generated_h_file_%s' % (filename_h, generated_I_string,generated_D_string, base_filename)) != 0:
        logging.info("preprocessing of the file %s using cpp failed" % filename_h)     
        # exit() #TODO remove comments after figuring out all the I paths


    # NOTE: Dont go through the path of turning of #ifdef by yourself , better to provide it externally


    # process the generated file again to remove them of __attributs__ since pycparser cant handle those
    process_the_file_for_removing_some_symbols('_generated_h_file_%s' % base_filename)

    # now once the previous command generates a new file , we can start parsing the file for ds

    exit()

    
    # send the output to the file
    os.system('ctags -x --c-kinds=mslut _gen_h_file_%s > temp_output' % base_filename)
    file_handle = open("temp_output" , "r+")
    lines = file_handle.readlines() ; file_handle.close()

    # Collect all the symbols into this dictionary
    all_ds = list()



# parse c file after preprocessing from the cpp
def parse_file_and_give_data_strcuture(filename,I_path='.',defines=''):

    base_filename = os.path.basename(filename)
    logging.info("Parsing file : Filename give = %s | Base Filename = %s\ninclude_path = %s" % (filename, base_filename,I_path))

    # generate combined include path in case more than one include paths are given
    generated_I_string = ''
    for path in I_path:
        generated_I_string = generated_I_string + '  -I' + path;
    # generate defines to be passed to the program
    generated_D_string = ''
    for define in defines:
        generated_D_string = generated_D_string + ' -D' + define
    print (generated_D_string)
    print (generated_I_string)


    if os.system('cpp %s %s %s>& _generated_h_file_%s' % (filename, generated_I_string,generated_D_string, base_filename)) != 0:
        logging.info("preprocessing of the file %s using cpp failed" % filename)     
        # exit() #TODO remove comments after figuring out all the I paths


    # NOTE: Dont go through the path of turning of #ifdef by yourself , better to provide it externally


    # process the generated file again to remove them of __attributs__ since pycparser cant handle those
    process_the_file_for_removing_some_symbols('_generated_h_file_%s' % base_filename)

    # now once the previous command generates a new file , we can start parsing the file for ds

    exit()

    
    # send the output to the file
    os.system('ctags -x --c-kinds=mslut _gen_h_file_%s > temp_output' % base_filename)
    file_handle = open("temp_output" , "r+")
    lines = file_handle.readlines() ; file_handle.close()

    # Collect all the symbols into this dictionary
    all_ds = list()

    # break down each line into seperate components
    for line in lines:
        split_line = line.split()
        member_or_struct_name = split_line[0]
        type_of_tag = split_line[1] # whether member, struct, union, typdef
        line_number = split_line[2] # can be start or end line number, have to find out
        file_name = split_line[3]  # doesnt really help
        rest_of_string = ' '.join(split_line[4:]) # this is the imp part


        # search for struct , doesnt work for anonymous structs yet
        if type_of_tag == 'struct':
            struct_dict = parse_struct_and_its_members(member_or_struct_name,line_number,rest_of_string , base_filename)
            all_ds.append(struct_dict)

    # once we have the list of all ds , code for union and typedef needs to go before this
    # do recursive fill of the data for all the 
    iterate_over_all_ds_and_generate_graph_for_each_one(all_ds)

#input : generated filename
#output: remove some unwanted characters and then regenrate using same filename
def process_the_file_for_removing_some_symbols(base_filename):
    logging.info('Processing the file to remove some symbols which parser cant handle')
    file_handle = open(base_filename )
    updated_text = file_handle.read() ; file_handle.close()
    os.system('rm %s' % base_filename)


    # all the things that parser cant handle
    # updated_text = re.sub(r'__attribute__.*\(\([_a-zA-Z\(0-9,\ ]+\)\)' , ' ' , updated_text)
    # special case with extra )
    # updated_text = re.sub(r'__attribute__.*\(\([_a-zA-Z\(\)0-9,\ ]+[\)]{2}' , ' ' , updated_text)
    updated_text = re.sub(r'__attribute__.*\(\([a-zA-Z]+\){2}' , ' ' , updated_text)
    updated_text = re.sub(r'__attribute__.*\(\(.*\){2}' , ' ' , updated_text)
    updated_text = re.sub(r'__const' , ' ' , updated_text )
    updated_text = re.sub(r'__extension__' , ' ' , updated_text )
    updated_text = re.sub(r'__inline(__)?' , ' ' , updated_text )
    updated_text = re.sub(r'__asm(__)?.*\(.+\)' , ' ' , updated_text )
    # empty line
    updated_text = re.sub(r'^[\ ].*;[\ ].*$' , ' ' , updated_text )
    # acc to gcc manual, built in type , have to special case it
    updated_text = re.sub(r'__builtin_va_list' , 'int' , updated_text )
    updated_text = re.sub(r'__restrict' , ' ' , updated_text)
    updated_text = re.sub(r'\# \d+ \".*\".*' , ' ', updated_text)



    file_handle = open('%s' % base_filename , 'w+')
    file_handle.write(updated_text)
    file_handle.close()

def pre_work():
    # Remove Logger from last time
    os.system("rm data.log")

    # Start Logger 
    logging.basicConfig(filename="data.log" , level=logging.DEBUG)

    # Get ignore list of the data structures you want to ignore
    # TODO




if __name__ == '__main__':

    pre_work()

    # Parse all the arguments
    parser = argparse.ArgumentParser("Description of the SCDB program")
    parser.add_argument('-i','--filename_h',help='Name of the .h file')
    parser.add_argument('-f','--filename_c',help='Name of the .c file')
    parser.add_argument('-I','--include_path',help='Path of include files', nargs='+')
    parser.add_argument('-D','--defines',help='Defines to be included', nargs='+')

    # Parse all the arguments
    args = vars(parser.parse_args())



    # 
    # This is the most basic usage that i want to solve
    #

    # get the list of defines
    if args['defines'] !=None:
        defines = args['defines']
    else:
        defines = ''

    # get the include path (for testing provide manually , but for generic case figure out from the makefile)
    if args['include_path'] != None:
        include_path = args['include_path']


    '''
    # if .h file is given that means data structure parsing 
    if args['filename_h'] != None:
        file_name_h = args['filename_h']
        
        if file_name_h != None:
            parse_h_file_and_give_data_strcuture(file_name_h, include_path, defines)
            exit()
        else:
            parse_h_file_and_give_data_strcuture(file_name_h)
            exit()
    '''

    # do the same for c file
    if args['filename_c'] != None:
        filename_c = args['filename_c']
        parse_file_and_give_data_strcuture(filename_c, include_path, defines)
