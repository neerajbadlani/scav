import os
import re
import pprint
import json
# filename_h : complete path from the root
def parse_h_file_and_give_data_strcuture(filename_h,I_path='.'):
    
    base_filename = os.path.basename(filename_h)
    # do preprocessing of the *.h file , so that it has all the info
    # if .h file has included something , we need to give the -I flags
    os.system('cpp %s -I%s > _generated_h_file_%s' % (filename_h, I_path, base_filename))
    # make it pass through clang-format to clean up format and make it easier to format
    os.system('clang-format _generated_h_file_%s > _gen_h_file_%s' % (base_filename, base_filename))
    os.system('rm _generated_h_file_%s' % base_filename)



    # send the output to the file
    os.system('ctags -x --c-kinds=mslut _gen_h_file_%s > temp_output' % base_filename)
    file_handle = open("temp_output" , "r+")
    lines = file_handle.readlines() ; file_handle.close()

    # Collect all the symbols into this dictionary
    all_ds = list()

    # break down each line into seperate components
    for line in lines:
        split_line = line.split()
        member_or_struct_name = split_line[0]
        type_of_tag = split_line[1] # whether member, struct, union, typdef
        line_number = split_line[2] # can be start or end line number, have to find out
        file_name = split_line[3]  # doesnt really help
        rest_of_string = ' '.join(split_line[4:]) # this is the imp part


        # search for struct , doesnt work for anonymous structs yet
        if type_of_tag == 'struct':
            struct_dict = parse_struct_and_its_members(member_or_struct_name,line_number,rest_of_string , base_filename)
            all_ds.append(struct_dict)

    # once we have the list of all ds , code for union and typedef needs to go before this
    # do recursive fill of the data for all the 
    iterate_over_all_ds_and_generate_graph_for_each_one(all_ds)


def iterate_over_all_ds_and_generate_graph_for_each_one(all_ds):

    for ds in all_ds:
        print (json.dumps(ds, indent=2))
        print ('\n'*3)

    # print out the graph for all the data structures in the graph
    for ds in all_ds:





def parse_struct_and_its_members(member_or_struct_name, line_number, rest_of_string, base_filename):
    # print (member_or_struct_name ,line_number, rest_of_string)
    # now we go to generated .h file and read it
    file_handle = open("_gen_h_file_%s" % base_filename  , "r+")
    lines = file_handle.readlines() ; file_handle.close()

    # create dict with just direct children
    struct_dict=dict()
    struct_members_dict = dict()
    struct_dict['type'] = 'struct ' + member_or_struct_name;
    struct_dict['children'] = list()

    # these line numbers are 1 less than what ctags gives you
    start_search_from_this_line = int(line_number) - 1
    end_search_at_this_line = start_search_from_this_line+1; 
    current_line = start_search_from_this_line+1;
    foundEndOfStruct = False
    while not foundEndOfStruct:
        struct_members_dict = dict()
        # search for '};' token in the line, that should suffice
        if '};' in lines[current_line]:
            foundEndOfStruct=True;
            end_search_at_this_line = current_line;
        else:
            symbol_name = lines[current_line].split()[-1][:-1] 
            struct_members_dict['name'] = symbol_name;
            struct_members_dict['type']=' '.join(lines[current_line].split()[:-1])
            # print ("**** " , struct_members_dict)
            
            struct_dict['children'].append(struct_members_dict)
            # print (struct_dict)
            current_line = current_line+1
    return (struct_dict)
    
    

    

if __name__ == '__main__':
    parse_h_file_and_give_data_strcuture('include/linux/sem.h')

